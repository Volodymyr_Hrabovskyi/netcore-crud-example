﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MVCApp.Infrastructure;
using MVCApp.Models;
using Newtonsoft.Json;

namespace MVCApp.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        public ProductsController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IActionResult> Index()
        {
            List<ProductViewModel> model = new List<ProductViewModel>();

            var client = _clientFactory.CreateClient("prod");

            HttpResponseMessage res = await client.GetAsync(API.Product.GetAll);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;

                model = JsonConvert.DeserializeObject<List<ProductViewModel>>(result);
            }

            List<CategoryViewModel> categories = new List<CategoryViewModel>();

            HttpResponseMessage response = await client.GetAsync(API.Category.GetAll);

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(result);
            }

            foreach(var product in model)
            {
                product.Category = categories.Where(x => x.CategoriesId == product.CategoryId).FirstOrDefault();
            }
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            List<CategoryViewModel> dto = new List<CategoryViewModel>();
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = await client.GetAsync(API.Category.GetAll);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                dto = JsonConvert.DeserializeObject<List<CategoryViewModel>>(result);
            }

            dto.Insert(0, new CategoryViewModel { CategoriesId = 0, Name = "--Select--" });

            ViewBag.ListofCategories = dto;

            return View();
        }
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                var client = _clientFactory.CreateClient("prod");

                var content = new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json");
                HttpRequestMessage message = new HttpRequestMessage();
                message.Content = content;
                message.Method = HttpMethod.Post;
                message.RequestUri = new Uri(API.Product.Add);
                HttpResponseMessage res = client.SendAsync(message).Result;
                if (res.IsSuccessStatusCode)
                {
                    return Redirect("~/Products/Index");
                }
            }

            return View(product);
        }

        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductViewModel model = new ProductViewModel();
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = await client.GetAsync(API.Product.GetById + id);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<ProductViewModel>(result);
            }

            if (model == null)
            {
                return NotFound();
            }

            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            var client1 = _clientFactory.CreateClient("prod");
            HttpResponseMessage response = await client.GetAsync(API.Category.GetAll);

            if (res.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(result);
            }

            categories.Insert(0, new CategoryViewModel { CategoriesId = 0, Name = "--Select--" });

            ViewBag.ListofCategories = categories;

            return View(model);
        }
  
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(long id, ProductViewModel product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var client = _clientFactory.CreateClient("prod");

                var content = new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json");
                HttpResponseMessage res = client.PutAsync(API.Product.Update + id, content).Result;

                if (res.IsSuccessStatusCode)
                {
                    return Redirect("~/Products/Index");
                }
            }

            return View(product);
        }
 
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductViewModel model = new ProductViewModel();
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = await client.GetAsync(API.Product.GetById + id);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<ProductViewModel>(result);
            }

            if (model == null)
            {
                return NotFound();
            }

            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            var client1 = _clientFactory.CreateClient("prod");
            HttpResponseMessage response = await client.GetAsync(API.Category.GetAll);

            if (res.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(result);
            }

            categories.Insert(0, new CategoryViewModel { CategoriesId = 0, Name = "--Select--" });

            ViewBag.ListofCategories = categories;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(ProductViewModel product)
        {
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = client.DeleteAsync(API.Product.Delete + product.ProductId).Result;
            if (res.IsSuccessStatusCode)
            {
                return Redirect("~/Products/Index");
            }

            return NotFound();
        }
    }
}