﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MVCApp.Infrastructure;
using MVCApp.Models;
using Newtonsoft.Json;
using WebAPI.Models;
using WebAPI.Services.Interfaces;

namespace MVCApp.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        public CategoriesController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IActionResult> Index()
        {
            List<CategoryViewModel> model = new List<CategoryViewModel>();

            var client = _clientFactory.CreateClient("prod");

            HttpResponseMessage res = await client.GetAsync(API.Category.GetAll);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;

                model = JsonConvert.DeserializeObject<List<CategoryViewModel>>(result);

            }
            return View(model);
        }
  
        public IActionResult Create()
        {
            return View();
        }
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                var client = _clientFactory.CreateClient("prod");

                var content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
                HttpRequestMessage message = new HttpRequestMessage();
                message.Content = content;
                message.Method = HttpMethod.Post;
                message.RequestUri = new Uri(API.Category.Add);
                HttpResponseMessage res = client.SendAsync(message).Result;
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            return View(category);
        }
 
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CategoryViewModel model = new CategoryViewModel();
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = await client.GetAsync(API.Category.GetById + id);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<CategoryViewModel>(result);
            }

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(long id, CategoryViewModel category)
        {
            if (id != category.CategoriesId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var client = _clientFactory.CreateClient("prod");

                var content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
                HttpResponseMessage res = client.PutAsync(API.Category.Update + id, content).Result;
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(category);
        }
  
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CategoryViewModel model = new CategoryViewModel();
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = await client.GetAsync(API.Category.GetById + id);

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<CategoryViewModel>(result);
            }

            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(CategoryViewModel category)
        {
            var client = _clientFactory.CreateClient("prod");
            HttpResponseMessage res = client.DeleteAsync(API.Category.Delete + category.CategoriesId).Result;
            if (res.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return NotFound();
        }
    }
}