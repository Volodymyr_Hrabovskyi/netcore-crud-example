﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCApp.Models
{
    public class CategoryViewModel
    {
        [Display(Name = "Category ID")]
        public long CategoriesId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Descriprion")]
        public string Description { get; set; }

        public ICollection<ProductViewModel> ProductsDTO { get; set; }

    }
}
