﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCApp.Models
{
    public class ProductViewModel
    {
        [Display(Name = "Product ID")]
        public long ProductId { get; set; }

        public string Name { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }
        public string Description { get; set; }

        [Display(Name = "Category")]
        public long CategoryId { get; set; }

        public virtual CategoryViewModel Category { get; set; }
    }
}
