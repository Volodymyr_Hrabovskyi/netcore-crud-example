﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCApp.Infrastructure
{
    public static class API
    {
        public static class Product
        {
            public static string GetAll = "api/Products/GetAllProducts";
            public static string GetById = "api/Products/GetById/";
            public static string Add = "http://localhost:27614/api/Products/AddProduct";
            public static string Update = "api/Products/UpdateProduct/";
            public static string Delete = "api/Products/DeleteProduct/";

        }

        public static class Category
        {
            public static string GetAll = "api/Categories/GetAllCategories";
            public static string GetById = "api/Categories/GetById/";
            public static string Add = "http://localhost:27614/api/Categories/AddCategory";
            public static string Update = "api/Categories/UpdateCategory/";
            public static string Delete = "api/Categories/DeleteCategory/";
        }
    }
}
