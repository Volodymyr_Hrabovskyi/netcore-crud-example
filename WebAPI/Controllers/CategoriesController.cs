﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;
using WebAPI.Repository;
using WebAPI.Services.Interfaces;

namespace WebAPI.Controllers
{
    //[Route("api/Categories")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
      
        [Route("api/Categories/GetAllCategories")]
        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return _categoryService.GetAllCategoriesList();
        }

        [Route("api/Categories/GetById/{id}")]
        [HttpGet("{id}")]
        public Category Get(int id)
        {
            return _categoryService.GetCategoryById(id);
        }

        [Route("api/Categories/AddCategory")]
        [HttpPost]
        public void Post([FromBody] Category category)
        {
            _categoryService.CreateCategoryAsync(category);
        }

        [Route("api/Categories/UpdateCategory/{id}")]
        [HttpPut("{id}")]
        public void Put([FromBody] Category category)
        {
            _categoryService.UpdateCategory(category);
        }

        [Route("api/Categories/DeleteCategory/{id}")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _categoryService.DeleteCategory(id);
        }
    }
}