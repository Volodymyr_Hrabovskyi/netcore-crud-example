﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;
using WebAPI.Repository;
using WebAPI.Services.Interfaces;

namespace WebAPI.Controllers
{   
    //[Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [Route("api/Products/GetAllProducts")]
        [HttpGet]
        public IEnumerable<Product> GetAll()
        {
            return _productService.GetAllProductsList();
        }

        [Route("api/Products/GetById/{id}")]
        [HttpGet]
        public Product GetProductById(int id)
        {
            return _productService.GetProductById(id);
        }

        [Route("api/Products/AddProduct")]
        [HttpPost]
        public void AddProduct([FromBody] Product product)
        {
            _productService.CreateLeagueAsync(product);
        }

        [Route("api/Products/UpdateProduct/{id}")]
        [HttpPut]
        public void UpdateProduct([FromBody] Product product)
        {
           _productService.UpdateProduct(product);
        }

        [Route("api/Products/DeleteProduct/{id}")]
        [HttpDelete]
        public void Delete(int id)
        {
            _productService.DeleteProduct(id);
        }
    }
}