﻿using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface IProductRepository : IRepository<Product>
    {

    }
    public class ProductRepository : EFRespository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}
