﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services.Interfaces
{
    public interface ICategoryService
    {
        Task CreateCategoryAsync(Category newCategory);
        List<Category> GetAllCategoriesList();
        Category GetCategoryById(int categoryId);
        Task UpdateCategory(Category category);
        Task DeleteCategory(int id);
    }
}
