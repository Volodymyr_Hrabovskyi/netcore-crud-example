﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services.Interfaces
{
    public interface IProductService
    {
        Task CreateLeagueAsync(Product newProduct);
        List<Product> GetAllProductsList();
        Product GetProductById(int productId);       
        Task UpdateProduct(Product product);
        Task DeleteProduct(int productId);
    }
}
