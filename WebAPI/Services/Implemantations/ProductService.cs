﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Repository;
using WebAPI.Services.Interfaces;

namespace WebAPI.Services.Implemantations
{
    public class ProductService : IProductService 
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productService)
        {
            _productRepository = productService;
        }


        public async Task CreateLeagueAsync(Product newProduct)
        {
            _productRepository.Add(newProduct);
            _productRepository.Commit();
        }

        public List<Product> GetAllProductsList()
        {
            return _productRepository.GetAll();
        }

        public Product GetProductById(int productId)
        {
            return _productRepository.GetById(productId);
        }

        public async Task UpdateProduct(Product product)
        {
            _productRepository.Update(product);
            _productRepository.Commit();
        }

        public async Task DeleteProduct(int productId)
        {
            _productRepository.Delete(productId);
            _productRepository.Commit();
        }
    }
}
