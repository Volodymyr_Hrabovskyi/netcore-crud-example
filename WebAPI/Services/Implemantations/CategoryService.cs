﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Repository;
using WebAPI.Services.Interfaces;

namespace WebAPI.Services.Implemantations
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task CreateCategoryAsync(Category newCategory)
        {
            _categoryRepository.Add(newCategory);
            _categoryRepository.Commit();
        }

        public List<Category> GetAllCategoriesList()
        {
            return _categoryRepository.GetAll();
        }

        public Category GetCategoryById(int categoryId)
        {
            return _categoryRepository.GetById(categoryId);
        }

        public async Task UpdateCategory(Category category)
        {
            _categoryRepository.Update(category);
            _categoryRepository.Commit();
        }

        public async Task DeleteCategory(int categoryId)
        {
            _categoryRepository.Delete(categoryId);
            _categoryRepository.Commit();
        }
    }
}
