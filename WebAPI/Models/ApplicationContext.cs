﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace WebAPI.Models
{
    public partial class ApplicationContext : DbContext
    {
        private IConfiguration _config;

        public ApplicationContext(DbContextOptions<ApplicationContext> options, IConfiguration config) : base(options)
        {
            _config = config;
        }

        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Category> Category { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                    _config.GetConnectionString("ConnectionString:ProductsDB"));
            }
        }
    }
}
